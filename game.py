from random import randint

name = input("Hi! What is your name? ")

for guess_num in range(1,6):

    month_num = randint(1, 12)
    year_num = randint(1924, 2004)

    print("Guess", guess_num, name, "were you born in",
    month_num, "/", year_num, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    else:
        if guess_num == 5:
            print("I've got better things to do. Goodbye")
        else:
            print("Drat! Lemme try again!")
